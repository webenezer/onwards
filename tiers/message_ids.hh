#pragma once

#include<stdint.h>

using messageid_t=uint8_t;

messageid_t const Login=1;
messageid_t const Generate=2;
messageid_t const Keepalive=3;

