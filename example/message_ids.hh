#pragma once

#include <stdint.h>

using messageid_t = uint8_t;

messageid_t const messageid1 = 1;
messageid_t const messageid2 = 2;
messageid_t const messageid3 = 3;
messageid_t const messageid4 = 4;
